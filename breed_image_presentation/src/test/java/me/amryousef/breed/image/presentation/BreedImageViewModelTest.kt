package me.amryousef.breed.image.presentation

import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import me.amryousef.catsdogs.breed.image.domain.BreedImage
import me.amryousef.catsdogs.breed.image.domain.BreedImagePage
import me.amryousef.catsdogs.breed.image.domain.BreedImageRepository
import me.amryousef.catsdogs.lib.domain.DataFetchException
import me.amryousef.catsdogs.lib.presentation.LoadingMoreState
import me.amryousef.catsdogs.lib.test.TestDispatcherProvider
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch
import kotlin.test.assertEquals
import kotlin.test.assertIs
import kotlin.test.assertTrue

class BreedImageViewModelTest {
    private val testDispatcherProvider = TestDispatcherProvider()
    private val mockBreedImageRepository = mockk<BreedImageRepository>()
    private lateinit var subject: BreedImageViewModel

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcherProvider.coroutineDispatcher)
        subject = BreedImageViewModel(
            mockBreedImageRepository
        )
    }

    @Test
    fun `given BreedImageRepository fetching data when onBreedsSelected then loading more state is loading`() {
        val latch = CountDownLatch(1)
        coEvery {
            mockBreedImageRepository.getAllBreeds(
                any(),
                any(),
                any()
            )
        }.answers {
            val state = subject.state.value
            assertIs<ImageScreenState.Ready>(state)
            assertIs<LoadingMoreState.Loading>(state.loadingMoreState)
            latch.countDown()
            return@answers BreedImagePage(emptyList(), 0)
        }
        subject.onBreedsSelected(
            listOf(
                SelectedBreed("", "", "")
            )
        )
        assertEquals(
            expected = 0,
            actual = latch.count
        )
    }

    @Test
    fun `given BreedImageRepository fails with initial state when onBreedsSelected then state is error`() {
        coEvery {
            mockBreedImageRepository.getAllBreeds(
                any(),
                any(),
                any()
            )
        }.throws(DataFetchException())
        subject.onBreedsSelected(
            listOf(
                SelectedBreed("", "", "")
            )
        )
        assertIs<ImageScreenState.Error>(subject.state.value)
    }

    @Test
    fun `given BreedImageRepository fails when onBreedsSelected then loading more state is error`() {
        coEvery {
            mockBreedImageRepository.getAllBreeds(
                any(),
                any(),
                any()
            )
        }.returns(
            BreedImagePage(
                data = listOf(
                    BreedImage("test", "http")
                ),
                totalCount = 10
            )
        )
        subject.onBreedsSelected(
            listOf(
                SelectedBreed("test", "test", "test")
            )
        )
        assertIs<ImageScreenState.Ready>(subject.state.value)
        coEvery {
            mockBreedImageRepository.getAllBreeds(
                any(),
                any(),
                any()
            )
        }.throws(DataFetchException())
        subject.onBreedsSelected(
            listOf(
                SelectedBreed("", "", "")
            )
        )
        val state = subject.state.value
        assertIs<ImageScreenState.Ready>(state)
        assertIs<LoadingMoreState.Error>(state.loadingMoreState)
    }

    @Test
    fun `given BreedImageRepository success when onBreedsSelected then state is ready`() {
        coEvery {
            mockBreedImageRepository.getAllBreeds(
                any(),
                any(),
                any()
            )
        }.returns(
            BreedImagePage(
                data = listOf(
                    BreedImage("test", "http"),
                ),
                totalCount = 10
            )
        )
        subject.onBreedsSelected(
            listOf(
                SelectedBreed("test", "test", "test"),
                SelectedBreed("test2", "test", "test")
            )
        )
        val state = subject.state.value
        assertIs<ImageScreenState.Ready>(state)
        assertEquals(
            expected = 2,
            actual = state.images.size
        )
        assertTrue {
            listOf(
                BreedImageState(
                    id = "test",
                    url = "http",
                    SelectedBreed("test", "test", "test")
                ),
                BreedImageState(
                    id = "test",
                    url = "http",
                    SelectedBreed("test2", "test", "test")
                )
            ).all { state.images.contains(it) }
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}