package me.amryousef.breed.image.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import me.amryousef.catsdogs.breed.image.domain.BreedImageRepository
import me.amryousef.catsdogs.lib.presentation.LoadingMoreState
import javax.inject.Inject

@HiltViewModel
class BreedImageViewModel @Inject constructor(
    private val breedImageRepository: BreedImageRepository
) : ViewModel() {
    companion object {
        private const val IMAGE_LIMIT = 10
        private val initialState = ImageScreenState.Ready(
            emptyList(),
            LoadingMoreState.Idle(false)
        )
    }

    private val latestSelectedBreeds = mutableListOf<SelectedBreed>()
    private val _state = MutableStateFlow<ImageScreenState>(initialState)
    val state = _state.asStateFlow()

    fun onBreedsSelected(breeds: List<SelectedBreed>) {
        latestSelectedBreeds.clear()
        latestSelectedBreeds.addAll(breeds)
        loadData()
    }

    private fun loadData() {
        val exceptionHandler = CoroutineExceptionHandler { _, _ ->
            viewModelScope.launch {
                doOnReady {
                    if (it.images.isEmpty()) {
                        _state.emit(ImageScreenState.Error)
                    } else {
                        _state.emit(
                            it.copy(
                                loadingMoreState = LoadingMoreState.Error
                            )
                        )
                    }
                }
            }
        }
        viewModelScope.launch(exceptionHandler) {
            doOnReady {
                _state.emit(
                    it.toggleLoadingMoreState(false)
                )
            }
            val images = latestSelectedBreeds.map { breed ->
                async {
                    breed to breedImageRepository.getAllBreeds(
                        0,
                        IMAGE_LIMIT,
                        breed.id
                    ).data
                }
            }.awaitAll().map {
                it.second.map { image ->
                    BreedImageState(
                        image.id,
                        image.url,
                        it.first
                    )
                }
            }.flatten().shuffled()
            doOnReady {
                _state.emit(
                    it.copy(
                        images = images
                    ).toggleLoadingMoreState(false)
                )
            }
        }
    }

    fun onRetryClicked() {
        viewModelScope.launch {
            if (_state.value is ImageScreenState.Error) {
                _state.emit(initialState)
            }
            loadData()
        }
    }

    private suspend fun doOnReady(block: suspend (ImageScreenState.Ready) -> Unit) {
        (_state.value as? ImageScreenState.Ready)?.let { block(it) }
    }
}