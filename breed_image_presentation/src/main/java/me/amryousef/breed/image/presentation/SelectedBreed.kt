package me.amryousef.breed.image.presentation

data class SelectedBreed(
    val id: String,
    val name: String,
    val origin: String
)