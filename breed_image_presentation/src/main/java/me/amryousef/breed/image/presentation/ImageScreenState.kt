package me.amryousef.breed.image.presentation

import me.amryousef.catsdogs.lib.presentation.LoadingMoreState

sealed class ImageScreenState {
    object Error : ImageScreenState()
    data class Ready(
        val images: List<BreedImageState>,
        val loadingMoreState: LoadingMoreState
    ) : ImageScreenState() {
        fun toggleLoadingMoreState(hasMore: Boolean): Ready =
            copy(
                loadingMoreState = loadingMoreState.toggle(hasMore)
            )
    }
}

data class BreedImageState(
    val id: String,
    val url: String,
    val breed: SelectedBreed
)