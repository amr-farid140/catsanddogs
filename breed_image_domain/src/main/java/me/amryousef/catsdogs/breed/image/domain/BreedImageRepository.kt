package me.amryousef.catsdogs.breed.image.domain

interface BreedImageRepository {
    suspend fun getAllBreeds(
        page: Int,
        limit: Int,
        breedId: String
    ): BreedImagePage
}