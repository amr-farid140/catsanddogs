package me.amryousef.catsdogs.breed.image.domain

data class BreedImage(
    val id: String,
    val url: String
)