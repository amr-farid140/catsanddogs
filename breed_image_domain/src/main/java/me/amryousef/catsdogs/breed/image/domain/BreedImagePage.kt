package me.amryousef.catsdogs.breed.image.domain

data class BreedImagePage(
    val data: List<BreedImage>,
    val totalCount: Int
)