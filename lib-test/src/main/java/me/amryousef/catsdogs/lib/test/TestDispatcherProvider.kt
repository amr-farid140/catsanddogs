package me.amryousef.catsdogs.lib.test

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineDispatcher
import me.amryousef.catsdogs.lib.domain.DispatchProvider

class TestDispatcherProvider(
    val coroutineDispatcher: CoroutineDispatcher = TestCoroutineDispatcher()
) : DispatchProvider {

    override fun io(): CoroutineDispatcher {
        return coroutineDispatcher
    }

    override fun ui(): CoroutineDispatcher {
        return coroutineDispatcher
    }

    override fun background(): CoroutineDispatcher {
        return coroutineDispatcher
    }
}