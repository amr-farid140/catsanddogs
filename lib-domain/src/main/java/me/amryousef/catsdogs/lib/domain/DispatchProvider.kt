package me.amryousef.catsdogs.lib.domain

import kotlinx.coroutines.CoroutineDispatcher

interface DispatchProvider {
    fun io(): CoroutineDispatcher
    fun ui(): CoroutineDispatcher
    fun background(): CoroutineDispatcher
}