package me.amryousef.catsdogs.breed.domain

data class BreedPage(
    val data: List<Breed>,
    val totalCount: Int
)