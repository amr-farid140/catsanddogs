package me.amryousef.catsdogs.breed.domain

data class BreedLifespan(
    val lowerBound: Int,
    val upperBound: Int
)