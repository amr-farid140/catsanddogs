package me.amryousef.catsdogs.breed.domain

interface BreedRepository {
    suspend fun getBreeds(
        page: Int = 0,
        limit: Int = 10
    ): BreedPage

    suspend fun searchBreedsByName(
        query: String
    ): List<Breed>
}