package me.amryousef.catsdogs.breed.domain

data class Breed(
    val id: String,
    val name: String,
    val origin: String,
    val lifespan: BreedLifespan
)