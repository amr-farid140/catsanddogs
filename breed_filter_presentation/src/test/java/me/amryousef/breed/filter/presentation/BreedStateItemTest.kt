package me.amryousef.breed.filter.presentation

import org.junit.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class BreedStateItemTest {
    @Test
    fun `given item is checked when toggleCheckedState then item is not checked`() {
        assertFalse(
            BreedStateItem(
                checked = true,
                "",
                "",
                "",
                BreedLifeSpanState(0, 0)
            ).toggleCheckedState().checked
        )
    }

    @Test
    fun `given item is not checked when toggleCheckedState then item is checked`() {
        assertTrue(
            BreedStateItem(
                checked = false,
                "",
                "",
                "",
                BreedLifeSpanState(0, 0)
            ).toggleCheckedState().checked
        )
    }
}