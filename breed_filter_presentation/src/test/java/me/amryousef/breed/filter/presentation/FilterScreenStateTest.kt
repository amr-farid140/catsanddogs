package me.amryousef.breed.filter.presentation

import io.mockk.spyk
import io.mockk.verify
import org.junit.Test
import kotlin.test.assertEquals

class FilterScreenStateTest {

    private val loadingMoreState = spyk(LoadingMoreState.Loading)
    private val baseInput =
        FilterScreenState.Ready(
            0,
            listOf(
                BreedStateItem(
                    true,
                    "",
                    "",
                    "",
                    BreedLifeSpanState(0, 0)
                )
            ),
            loadingMoreState
        )

    @Test
    fun `given ready state when updateCurrentPage then currentPage is updated`() {
        assertEquals(
            expected = baseInput.updateCurrentPage(2).currentPage,
            actual = 2
        )
    }

    @Test
    fun `given ready state when appendBreeds then breeds are appended`() {
        assertEquals(
            expected = baseInput.appendBreeds(
                listOf(
                    BreedStateItem(
                        checked = true,
                        "",
                        "",
                        "",
                        BreedLifeSpanState(0, 0)
                    )
                )
            ).breeds.size,
            actual = 2
        )
    }

    @Test
    fun `given ready state when toggleLoadingMoreState then LoadingMoreState-toggle is called`() {
        baseInput.toggleLoadingMoreState(true)
        verify { loadingMoreState.toggle(true) }
    }

    @Test
    fun `given ready state when toggleBreedCheckedState then checked is toggled`() {
        val itemsList = listOf(
            spyk(
                BreedStateItem(
                    checked = true,
                    "",
                    "",
                    "",
                    BreedLifeSpanState(0, 0)
                )
            )
        )
        baseInput
            .appendBreeds(itemsList)
            .toggleBreedCheckedState(itemsList.first())

        verify { itemsList.first().toggleCheckedState() }
    }
}