package me.amryousef.breed.filter.presentation

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import me.amryousef.catsdogs.breed.domain.Breed
import me.amryousef.catsdogs.breed.domain.BreedLifespan
import me.amryousef.catsdogs.breed.domain.BreedPage
import me.amryousef.catsdogs.breed.domain.BreedRepository
import me.amryousef.catsdogs.lib.domain.DataFetchException
import me.amryousef.catsdogs.lib.test.TestDispatcherProvider
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertIs
import kotlin.test.assertTrue

class BreedFilterViewModelTest {
    private val mockBreedRepository = mockk<BreedRepository> {
        coEvery { getBreeds(any(), any()) } returns BreedPage(emptyList(), 1)
    }
    private lateinit var subject: BreedFilterViewModel
    private val testDispatcherProvider = TestDispatcherProvider()

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcherProvider.coroutineDispatcher)
    }

    @Test
    fun `given breedRepository when view model is created then first page is fetched`() {
        subject = BreedFilterViewModel(
            breedRepository = mockBreedRepository
        )
        coVerify { mockBreedRepository.getBreeds(0, 30) }
    }

    @Test
    fun `given breedRepository when onLoadMoreBreeds then loading more state is loading`() =
        runBlockingTest(testDispatcherProvider.coroutineDispatcher) {
            val latch = CountDownLatch(1)
            subject = BreedFilterViewModel(
                breedRepository = mockBreedRepository
            )
            coEvery {
                mockBreedRepository.getBreeds(any(), any())
            }.answers {
                val state = subject.state.value
                assertIs<FilterScreenState.Ready>(state)
                assertIs<LoadingMoreState.Loading>(state.loadingMoreState)
                latch.countDown()
                return@answers BreedPage(emptyList(), 1)
            }
            subject.onLoadMoreBreeds()
            assertEquals(
                actual = latch.count,
                expected = 0
            )
        }

    @Test
    fun `given breedRepository fails on initial load when onLoadMoreBreeds then state is error`() =
        runBlockingTest(testDispatcherProvider.coroutineDispatcher) {
            coEvery {
                mockBreedRepository.getBreeds(any(), any())
            }.throws(DataFetchException())
            subject = BreedFilterViewModel(
                breedRepository = mockBreedRepository
            )
            assertIs<FilterScreenState.Error>(subject.state.value)
        }

    @Test
    fun `given breedRepository fails on subsequent load when onLoadMoreBreeds then loading more state is error`() =
        runBlockingTest(testDispatcherProvider.coroutineDispatcher) {
            subject = BreedFilterViewModel(
                breedRepository = mockBreedRepository
            )
            coEvery {
                mockBreedRepository.getBreeds(any(), any())
            }.throws(DataFetchException())
            subject.onLoadMoreBreeds()
            val state = subject.state.value
            assertIs<FilterScreenState.Ready>(state)
            assertIs<LoadingMoreState.Error>(state.loadingMoreState)
        }

    @Test
    fun `given breedRepository success when onLoadMoreBreeds then state is ready with loading state idle`() =
        runBlockingTest(testDispatcherProvider.coroutineDispatcher) {
            coEvery {
                mockBreedRepository.getBreeds(any(), any())
            }.returns(
                BreedPage(
                    data = (0..29).map {
                        Breed(
                            id = it.toString(),
                            name = "${it}test",
                            origin = "${it}origin",
                            lifespan = BreedLifespan(0, it)
                        )
                    },
                    totalCount = 100
                )
            )
            subject = BreedFilterViewModel(
                breedRepository = mockBreedRepository
            )
            val state = subject.state.value
            assertIs<FilterScreenState.Ready>(state)
            assertEquals(
                expected = 30,
                actual = state.breeds.size
            )
            assertTrue(
                (state.loadingMoreState as LoadingMoreState.Idle).hasMore
            )
        }

    @Test
    fun `given breedRepository success with max total count when onLoadMoreBreeds then state is ready with loading state idle and load more is false`() =
        runBlockingTest(testDispatcherProvider.coroutineDispatcher) {
            coEvery {
                mockBreedRepository.getBreeds(any(), any())
            }.returns(
                BreedPage(
                    data = (0..29).map {
                        Breed(
                            id = it.toString(),
                            name = "${it}test",
                            origin = "${it}origin",
                            lifespan = BreedLifespan(0, it)
                        )
                    },
                    totalCount = 30
                )
            )
            subject = BreedFilterViewModel(
                breedRepository = mockBreedRepository
            )
            val state = subject.state.value
            assertIs<FilterScreenState.Ready>(state)
            assertEquals(
                expected = 30,
                actual = state.breeds.size
            )
            assertFalse(
                (state.loadingMoreState as LoadingMoreState.Idle).hasMore
            )
        }

    @Test
    fun `given error state when onRetryClicked then first page is fetched`() {
        coEvery { mockBreedRepository.getBreeds(any(), any()) } throws DataFetchException()
        subject = BreedFilterViewModel(
            breedRepository = mockBreedRepository
        )
        subject.onRetryClicked()
        coVerify(exactly = 2) { mockBreedRepository.getBreeds(0, 30) }
    }

    @Test
    fun `given ready state when onBreedClicked then breed checked state is toggled`() {
        coEvery {
            mockBreedRepository.getBreeds(any(), any())
        } returns BreedPage(
            data = listOf(
                Breed(
                    id = "1",
                    name = "test",
                    origin = "origin",
                    lifespan = BreedLifespan(0, 10)
                )
            ),
            totalCount = 10
        )
        subject = BreedFilterViewModel(
            breedRepository = mockBreedRepository
        )
        subject.onBreedClicked(
            BreedStateItem(
                checked = false,
                id = "1",
                name = "test",
                origin = "origin",
                lifeSpanState = BreedLifeSpanState(0, 10)
            )
        )
        val state = subject.state.value
        assertIs<FilterScreenState.Ready>(state)
        assertTrue(state.breeds.first().checked)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}