package me.amryousef.breed.filter.presentation

import me.amryousef.catsdogs.lib.presentation.LoadingMoreState

sealed class FilterScreenState {
    object Error : FilterScreenState()
    data class Ready(
        val currentPage: Int,
        val breeds: List<BreedStateItem>,
        val loadingMoreState: LoadingMoreState
    ) : FilterScreenState() {
        fun updateCurrentPage(newCurrentPage: Int) =
            copy(
                currentPage = newCurrentPage
            )

        fun toggleLoadingMoreState(hasMore: Boolean): Ready =
            copy(
                loadingMoreState = loadingMoreState.toggle(hasMore)
            )

        fun appendBreeds(breeds: List<BreedStateItem>) = copy(
            breeds = this.breeds + breeds
        )

        fun toggleBreedCheckedState(breedStateItem: BreedStateItem) = copy(
            breeds = breeds.map {
                if (it == breedStateItem) {
                    breedStateItem.toggleCheckedState()
                } else {
                    it
                }
            }
        )
    }
}