package me.amryousef.breed.filter.presentation

data class BreedLifeSpanState(
    val lowerBound: Int,
    val upperBound: Int
)