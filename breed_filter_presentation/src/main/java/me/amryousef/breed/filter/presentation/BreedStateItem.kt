package me.amryousef.breed.filter.presentation

data class BreedStateItem(
    val checked: Boolean,
    val name: String,
    val id: String,
    val origin: String,
    val lifeSpanState: BreedLifeSpanState
) {
    fun toggleCheckedState(): BreedStateItem =
        copy(checked = !checked)
}