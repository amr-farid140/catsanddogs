package me.amryousef.breed.filter.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import me.amryousef.catsdogs.breed.domain.BreedRepository
import me.amryousef.catsdogs.lib.presentation.LoadingMoreState
import javax.inject.Inject

@HiltViewModel
class BreedFilterViewModel @Inject constructor(
    private val breedRepository: BreedRepository
) : ViewModel() {

    companion object {
        private const val PAGE_LIMIT = 30
        private val initialState = FilterScreenState.Ready(
            currentPage = -1,
            breeds = emptyList(),
            loadingMoreState = LoadingMoreState.Idle(true)
        )
    }

    private val _state = MutableStateFlow<FilterScreenState>(initialState)
    val state = _state.asStateFlow()

    init {
        onLoadMoreBreeds()
    }

    fun onRetryClicked() {
        viewModelScope.launch {
            _state.emit(initialState)
            onLoadMoreBreeds()
        }
    }

    fun onLoadMoreBreeds() {
        doOnReady { currentState ->
            val page = currentState.currentPage + 1
            viewModelScope.launch {
                val updatedState = currentState.toggleLoadingMoreState(true)
                _state.emit(updatedState)
                try {
                    val breeds = breedRepository.getBreeds(
                        page = page,
                        limit = PAGE_LIMIT
                    )
                    _state.emit(
                        updatedState.appendBreeds(
                            breeds.data.map { breed ->
                                BreedStateItem(
                                    checked = false,
                                    name = breed.name,
                                    origin = breed.origin,
                                    id = breed.id,
                                    lifeSpanState = BreedLifeSpanState(
                                        breed.lifespan.lowerBound,
                                        breed.lifespan.upperBound
                                    )
                                )
                            }
                        ).toggleLoadingMoreState(
                            hasMore = ((page + 1) * PAGE_LIMIT) < breeds.totalCount
                        ).updateCurrentPage(page)
                    )
                } catch (exception: Exception) {
                    if (currentState == initialState) {
                        _state.emit(FilterScreenState.Error)
                    } else {
                        _state.emit(
                            updatedState.copy(
                                loadingMoreState = LoadingMoreState.Error
                            )
                        )
                    }
                }
            }
        }
    }

    fun onBreedClicked(breed: BreedStateItem) {
        doOnReady { currentState ->
            _state.value = currentState.toggleBreedCheckedState(breed)
        }
    }

    private fun doOnReady(block: (FilterScreenState.Ready) -> Unit) {
        (_state.value as? FilterScreenState.Ready)?.let { block(it) }
    }
}