package me.amryousef.catsdogs.breed.image.data

import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.test.runBlockingTest
import me.amryousef.catsdogs.breed.image.domain.BreedImagePage
import me.amryousef.catsdogs.lib.domain.DataFetchException
import me.amryousef.catsdogs.lib.test.TestDispatcherProvider
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import retrofit2.Response

class RemoteBreedImageRepositoryTest {
    private val testDispatcherProvider = TestDispatcherProvider()
    private val mockBreedImageApiService = mockk<BreedImageApiService>()
    private val mockBreedImageMapper = mockk<BreedImageMapper>()
    private lateinit var subject: RemoteBreedImageRepository

    @Before
    fun setup() {
        subject = RemoteBreedImageRepository(
            dispatchProvider = testDispatcherProvider,
            breedImageApiService = mockBreedImageApiService,
            breedImageMapper = mockBreedImageMapper
        )
    }

    @Test(expected = DataFetchException::class)
    fun `given response is not successful when getAllBreeds then DataFetchException is thrown`() {
        runBlockingTest(testDispatcherProvider.coroutineDispatcher) {
            coEvery { mockBreedImageApiService.getImages(any(), any(), any()) }
                .returns(Response.error(400, ResponseBody.create(null, "test")))
            subject.getAllBreeds(0, 10, "test")
        }
    }

    @Test
    fun `given response is successful when getAllBreeds then data is mapped`() {
        runBlockingTest(testDispatcherProvider.coroutineDispatcher) {
            coEvery { mockBreedImageApiService.getImages(any(), any(), any()) }
                .returns(
                    Response.success(emptyList())
                )
            every { mockBreedImageMapper.map(any(), any()) }.returns(
                BreedImagePage(emptyList(), 0)
            )
            subject.getAllBreeds(0, 10, "test")
            verify { mockBreedImageMapper.map(any(), any()) }
        }
    }
}