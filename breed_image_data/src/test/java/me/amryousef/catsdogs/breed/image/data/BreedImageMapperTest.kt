package me.amryousef.catsdogs.breed.image.data

import me.amryousef.catsdogs.breed.image.domain.BreedImage
import me.amryousef.catsdogs.breed.image.domain.BreedImagePage
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class BreedImageMapperTest {
    private lateinit var subject: BreedImageMapper

    @Before
    fun setup() {
        subject = BreedImageMapper()
    }

    @Test
    fun `given response when map then response is mapped`() {
        val result = subject.map(
            response = listOf(
                BreedImageResponse(
                    id = "1",
                    url = "test"
                )
            ),
            null
        )
        assertEquals(
            expected = BreedImagePage(
                data = listOf(
                    BreedImage(
                        "1",
                        "test"
                    )
                ),
                totalCount = Int.MAX_VALUE
            ),
            actual = result
        )
    }
}