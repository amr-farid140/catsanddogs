package me.amryousef.catsdogs.breed.image.data

import me.amryousef.catsdogs.breed.image.domain.BreedImage
import me.amryousef.catsdogs.breed.image.domain.BreedImagePage
import javax.inject.Inject

class BreedImageMapper @Inject constructor() {
    fun map(
        response: List<BreedImageResponse>, totalCount: Int?
    ): BreedImagePage {
        return BreedImagePage(
            data = response.map { item ->
                BreedImage(
                    id = item.id,
                    url = item.url
                )
            },
            totalCount = totalCount ?: Int.MAX_VALUE
        )
    }
}