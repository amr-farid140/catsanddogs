package me.amryousef.catsdogs.breed.image.data

import kotlinx.coroutines.withContext
import me.amryousef.catsdogs.breed.image.domain.BreedImagePage
import me.amryousef.catsdogs.breed.image.domain.BreedImageRepository
import me.amryousef.catsdogs.lib.domain.DataFetchException
import me.amryousef.catsdogs.lib.domain.DispatchProvider
import javax.inject.Inject

class RemoteBreedImageRepository @Inject constructor(
    private val breedImageApiService: BreedImageApiService,
    private val breedImageMapper: BreedImageMapper,
    private val dispatchProvider: DispatchProvider
) : BreedImageRepository {

    companion object {
        private const val PAGINATION_COUNT_HEADER_KEY = "Pagination-Count"
    }

    override suspend fun getAllBreeds(
        page: Int,
        limit: Int,
        breedId: String
    ): BreedImagePage = withContext(dispatchProvider.io()) {
        val response = breedImageApiService.getImages(page, limit, breedId)
        if (response.isSuccessful) {
            breedImageMapper.map(
                response.body() ?: throw DataFetchException(),
                response.headers()[PAGINATION_COUNT_HEADER_KEY]?.toIntOrNull()
            )
        } else {
            throw DataFetchException()
        }
    }
}