package me.amryousef.catsdogs.breed.image.data

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface BreedImageApiService {
    @GET("/v1/images/search")
    suspend fun getImages(
        @Query("page") page: Int,
        @Query("limit") limit: Int,
        @Query("breed_id") breedId: String
    ): Response<List<BreedImageResponse>>
}