package me.amryousef.catsdogs.breed.image

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.MutableStateFlow
import me.amryousef.breed.image.presentation.BreedImageViewModel
import me.amryousef.breed.image.presentation.ImageScreenState
import me.amryousef.catsdogs.lib.presentation.LoadingMoreState
import org.junit.Rule
import org.junit.Test

class ImageListTest {
    @get:Rule
    val composeTestRule = createComposeRule()


    private val mockViewModel = mockk<BreedImageViewModel>(relaxed = true)

    @Test
    fun given_error_state_when_image_list_is_rendered_then_error_screen_is_displayed() {
        every { mockViewModel.state } returns MutableStateFlow(ImageScreenState.Error)
        composeTestRule.setContent {
            ImageList(viewModel = mockViewModel)
        }
        composeTestRule.onNodeWithText("Failed to load images").assertExists()
        composeTestRule.onNodeWithText("No images to display. Select more breeds from the side drawer to load more images").assertDoesNotExist()
    }

    @Test
    fun given_ready_state_when_image_list_is_rendered_then_ready_screen_is_displayed() {
        every { mockViewModel.state } returns MutableStateFlow(
            ImageScreenState.Ready(
                images = emptyList(),
                loadingMoreState = LoadingMoreState.Idle(false)
            )
        )
        composeTestRule.setContent {
            ImageList(viewModel = mockViewModel)
        }
        composeTestRule.onNodeWithTag("ready_screen").assertExists()
        composeTestRule.onNodeWithText("Failed to load images").assertDoesNotExist()
    }
}