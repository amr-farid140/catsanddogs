package me.amryousef.catsdogs.breed.image

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTag
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import me.amryousef.breed.image.presentation.BreedImageViewModel
import me.amryousef.breed.image.presentation.ImageScreenState
import me.amryousef.catsdogs.breed.image.ui.R
import me.amryousef.catsdogs.lib.presentation.LoadingMoreState
import me.amryousef.catsdogs.lib.ui.ErrorScreen
import me.amryousef.lib.ui.theme.PaddingLarge
import me.amryousef.lib.ui.theme.PaddingMedium
import me.amryousef.lib.ui.theme.PaddingSmall
import me.amryousef.lib.ui.theme.PaddingTiny

@Composable
fun ImageList(
    modifier: Modifier = Modifier,
    viewModel: BreedImageViewModel
) {
    val state by viewModel.state.collectAsState()

    when (val currentState = state) {
        is ImageScreenState.Error ->
            ErrorScreen(
                modifier = modifier,
                errorMessage = stringResource(R.string.breed_image_list_error_message),
                buttonText = stringResource(R.string.breed_image_list_error_button_text)
            ) { viewModel.onRetryClicked() }
        is ImageScreenState.Ready -> ReadyScreen(
            modifier = modifier.semantics {
                testTag = "ready_screen"
            },
            state = currentState
        ) { viewModel.onRetryClicked() }
    }
}

@Composable
internal fun ReadyScreen(
    modifier: Modifier,
    state: ImageScreenState.Ready,
    onRetryClicked: () -> Unit
) {
    if (state.images.isEmpty()) {
        EmptyScreen(modifier = modifier)
    } else {
        BreedImages(modifier = modifier, state = state, onRetryClicked = onRetryClicked)
    }
}

@Composable
internal fun BreedImages(
    modifier: Modifier = Modifier,
    state: ImageScreenState.Ready,
    onRetryClicked: () -> Unit
) {
    LazyColumn(
        modifier = modifier,
        contentPadding = PaddingValues(PaddingMedium),
        verticalArrangement = Arrangement.spacedBy(PaddingMedium)
    ) {
        if (state.loadingMoreState is LoadingMoreState.Error) {
            item {
                ErrorScreen(
                    modifier = Modifier.fillMaxWidth(),
                    errorMessage = stringResource(R.string.breed_image_list_error_message),
                    buttonText = stringResource(R.string.breed_image_list_error_button_text)
                ) {
                    onRetryClicked()
                }
            }
        }
        items(state.images, key = { it.hashCode() }) {
            Card(
                modifier = Modifier.fillMaxSize(),
                shape = RoundedCornerShape(4.dp)
            ) {
                Box(modifier = Modifier.fillMaxSize()) {
                    Image(
                        painter = rememberImagePainter(it.url),
                        contentDescription = null,
                        contentScale = ContentScale.FillWidth,
                        modifier = Modifier.height(240.dp)
                    )
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .align(Alignment.BottomStart)
                            .background(
                                Color.Black.copy(
                                    alpha = 0.3f
                                )
                            )
                            .padding(PaddingSmall),
                    ) {
                        Text(
                            text = it.breed.name,
                            style = MaterialTheme.typography.subtitle1.copy(
                                color = MaterialTheme.colors.background
                            )
                        )
                        Spacer(modifier = Modifier.height(PaddingTiny))
                        Text(
                            text = it.breed.origin,
                            style = MaterialTheme.typography.body2.copy(
                                color = MaterialTheme.colors.background
                            )
                        )
                    }

                }
            }
        }
        if (state.loadingMoreState is LoadingMoreState.Loading) {
            item {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center
                ) {
                    CircularProgressIndicator()
                }
            }
        }
    }
}

@Composable
internal fun EmptyScreen(
    modifier: Modifier = Modifier
) {
    Box(
        modifier = modifier.padding(horizontal = PaddingLarge),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = stringResource(R.string.breed_image_list_empty_list_message),
            textAlign = TextAlign.Center
        )
    }
}