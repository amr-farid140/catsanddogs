# Cats and Dogs

This Android application allows users to selected one or more breed to see images of selected breeds.

Users can select breeds from the side drawer. Every change in selection will update the list of images and shuffle it.

This app only supports Cats API at the moment.

## Tools used

- Kotlin
- Coroutines for concurrency 
- Jetpack Compose for UI
- Dagger & Hilt for Dependency injection
- Retrofit for handling HTTP requests
- Moshi for JSON serialisation

## Architecture & Code structure

This project is adopts a multi-module structure where each feature is split into 4 modules
- Domain: A kotlin only module to define business logic API contracts and models
- Data: An android module to serve as data access layer and provide platform-dependent implementations for API contracts defined in domain.
- Presentation: An android module to house the Android ViewModel which is a state reducer and action handler. In theory this could be platform agnostic but we need some Android framework classes since we are relying on `ViewModel` supertype.
- UI: An Android module to house all UI elements related to specific feature

In addition to feature modules, this project has multiple common-logic modules which are usually prefixed with `lib`. For example:
- `lib-ui` contains common views
- `lib-presentation` contains common state reduction models and logic
- `lib-test` contains utilities to facilitate unit tests

### Presentation Layer Architecture

The presentation layer uses a uni-directional data flow where Views create `actions` by invoking functions on a `ViewModel` class. The `ViewModel` class acts as a state reducer by delegating work to the domain layer and computing the next state update.
Views listen to a `StateFlow` to receive UI state updates.