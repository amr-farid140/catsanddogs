package me.amryousef.catsdogs.lib.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import me.amryousef.lib.ui.theme.PaddingSmall

@Composable
fun ErrorScreen(
    modifier: Modifier = Modifier,
    errorMessage: String,
    buttonText: String,
    onRetryClicked: () -> Unit
) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = errorMessage)
        Spacer(modifier = Modifier.height(PaddingSmall))
        Button(onClick = { onRetryClicked() }) {
            Text(text = buttonText)
        }
    }
}