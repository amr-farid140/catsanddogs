package me.amryousef.lib.ui.theme

import androidx.compose.ui.unit.dp

val PaddingTiny = 2.dp
val PaddingSmall = 4.dp
val PaddingMedium = 8.dp
val PaddingLarge = 12.dp
val PaddingXLarge = 16.dp
val PaddingHuge = 18.dp