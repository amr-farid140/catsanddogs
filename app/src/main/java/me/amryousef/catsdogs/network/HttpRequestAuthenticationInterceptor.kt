package me.amryousef.catsdogs.network

import okhttp3.Interceptor
import okhttp3.Response

class HttpRequestAuthenticationInterceptor : Interceptor {
    companion object {
        private const val API_KEY = "3440e048-7cf1-4667-9170-2d176346ceef"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
            .newBuilder()
            .addHeader("x-api-key", API_KEY)
            .build()
        return chain.proceed(request)
    }
}