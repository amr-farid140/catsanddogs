package me.amryousef.catsdogs

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Menu
import androidx.compose.material.icons.rounded.Star
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import me.amryousef.breed.filter.presentation.BreedFilterViewModel
import me.amryousef.breed.image.presentation.BreedImageViewModel
import me.amryousef.breed.image.presentation.SelectedBreed
import me.amryousef.catsdogs.breed.filter.BreedsFilter
import me.amryousef.catsdogs.breed.image.ImageList
import me.amryousef.lib.ui.theme.CatsDogsTheme
import me.amryousef.lib.ui.theme.PaddingMedium

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CatsDogsTheme {
                val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
                val coroutineScope = rememberCoroutineScope()
                val scaffoldState = rememberScaffoldState(drawerState = drawerState)
                val breedsFilterViewModel by viewModels<BreedFilterViewModel>()
                val breedsImageViewModel by viewModels<BreedImageViewModel>()
                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = { Text(getString(R.string.main_top_bar_title)) },
                            navigationIcon = {
                                IconButton(
                                    onClick = {
                                        if (drawerState.isOpen) {
                                            coroutineScope.launch { drawerState.close() }
                                        } else {
                                            coroutineScope.launch { drawerState.open() }
                                        }
                                    }
                                ) {
                                    Icon(
                                        imageVector = Icons.Rounded.Menu,
                                        contentDescription = if (drawerState.isOpen) {
                                            getString(R.string.main_side_drawer_close_content_description)
                                        } else {
                                            getString(R.string.main_side_drawer_open_content_description)
                                        }
                                    )
                                }
                            }
                        )
                    },
                    drawerGesturesEnabled = true,
                    drawerContent = {
                        BreedsFilter(
                            modifier = Modifier.fillMaxSize(),
                            breedsFilterViewModel = breedsFilterViewModel
                        ) { selectedBreeds ->
                            breedsImageViewModel
                                .onBreedsSelected(
                                    selectedBreeds.map {
                                        SelectedBreed(
                                            id = it.id,
                                            name = it.name,
                                            origin = it.origin
                                        )
                                    }
                                )
                        }
                    },
                    scaffoldState = scaffoldState
                ) {
                    ImageList(
                        modifier = Modifier.fillMaxSize(),
                        viewModel = breedsImageViewModel
                    )
                }
            }
        }
    }
}