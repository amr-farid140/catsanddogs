package me.amryousef.catsdogs

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CatsDogsApplication: Application()