package me.amryousef.catsdogs.di

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import me.amryousef.catsdogs.BuildConfig
import me.amryousef.catsdogs.breed.data.BreedApiService
import me.amryousef.catsdogs.breed.image.data.BreedImageApiService
import me.amryousef.catsdogs.network.HttpRequestAuthenticationInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(HttpRequestAuthenticationInterceptor())
            .apply {
                if (BuildConfig.DEBUG) {
                    addInterceptor(
                        HttpLoggingInterceptor().apply {
                            level = HttpLoggingInterceptor.Level.BODY
                        }
                    )
                }
            }
            .build()

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder().build()

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        moshi: Moshi
    ): Retrofit = Retrofit.Builder()
        .baseUrl("https://api.thecatapi.com/")
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()

    @Provides
    @Singleton
    fun provideBreedsApiService(
        retrofit: Retrofit
    ): BreedApiService = retrofit.create(BreedApiService::class.java)

    @Provides
    @Singleton
    fun provideBreedsImageApiService(
        retrofit: Retrofit
    ): BreedImageApiService = retrofit.create(BreedImageApiService::class.java)
}