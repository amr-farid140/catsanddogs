package me.amryousef.catsdogs.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import me.amryousef.catsdogs.AndroidDispatcherProvider
import me.amryousef.catsdogs.breed.data.RemoteBreedRepository
import me.amryousef.catsdogs.breed.domain.BreedRepository
import me.amryousef.catsdogs.breed.image.data.RemoteBreedImageRepository
import me.amryousef.catsdogs.breed.image.domain.BreedImageRepository
import me.amryousef.catsdogs.lib.domain.DispatchProvider
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DomainModule {
    @Singleton
    @Binds
    abstract fun bindBreedRepository(
        remoteBreedRepository: RemoteBreedRepository
    ): BreedRepository

    @Singleton
    @Binds
    abstract fun bindBreedImageRepository(
        remoteBreedRepository: RemoteBreedImageRepository
    ): BreedImageRepository

    @Singleton
    @Binds
    abstract fun bindDispatcherProvider(
        androidDispatcherProvider: AndroidDispatcherProvider
    ): DispatchProvider
}