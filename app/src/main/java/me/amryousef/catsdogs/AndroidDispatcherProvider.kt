package me.amryousef.catsdogs

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import me.amryousef.catsdogs.lib.domain.DispatchProvider
import javax.inject.Inject

class AndroidDispatcherProvider @Inject constructor() : DispatchProvider {
    override fun io(): CoroutineDispatcher = Dispatchers.IO

    override fun ui(): CoroutineDispatcher = Dispatchers.Main

    override fun background(): CoroutineDispatcher = Dispatchers.Default
}