package me.amryousef.catsdogs.lib.presentation

import org.junit.Test
import kotlin.test.assertIs

class LoadingMoreStateTest {
    @Test
    fun `given idle when toggle then loading is returned`() {
        val input = LoadingMoreState.Idle(false)
        val result = input.toggle(false)
        assertIs<LoadingMoreState.Loading>(result)
    }

    @Test
    fun `given loading when toggle then idle is returned`() {
        val input = LoadingMoreState.Loading
        val result = input.toggle(false)
        assertIs<LoadingMoreState.Idle>(result)
    }

    @Test
    fun `given error when toggle then loading is returned`() {
        val input = LoadingMoreState.Error
        val result = input.toggle(false)
        assertIs<LoadingMoreState.Loading>(result)
    }
}