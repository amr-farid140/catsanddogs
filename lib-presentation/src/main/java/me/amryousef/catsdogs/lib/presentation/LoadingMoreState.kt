package me.amryousef.catsdogs.lib.presentation

sealed class LoadingMoreState {
    data class Idle(val hasMore: Boolean) : LoadingMoreState()
    object Loading : LoadingMoreState()
    object Error: LoadingMoreState()

    fun toggle(hasMore: Boolean): LoadingMoreState = when (this) {
        is Loading -> Idle(hasMore)
        else -> Loading
    }
}