package me.amryousef.catsdogs.breed.filter

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTag
import me.amryousef.breed.filter.presentation.BreedFilterViewModel
import me.amryousef.breed.filter.presentation.BreedStateItem
import me.amryousef.breed.filter.presentation.FilterScreenState
import me.amryousef.catsdogs.breed.filter.ui.R
import me.amryousef.catsdogs.lib.presentation.LoadingMoreState
import me.amryousef.catsdogs.lib.ui.ErrorScreen
import me.amryousef.lib.ui.theme.PaddingMedium
import me.amryousef.lib.ui.theme.PaddingSmall

@Composable
fun BreedsFilter(
    modifier: Modifier = Modifier,
    breedsFilterViewModel: BreedFilterViewModel,
    onSelectedBreedsChanged: (List<BreedStateItem>) -> Unit
) {
    val state by breedsFilterViewModel.state.collectAsState()
    when (state) {
        is FilterScreenState.Error ->
            ErrorScreen(
                modifier = modifier,
                errorMessage = stringResource(id = R.string.breeds_filter_error_message),
                buttonText = stringResource(id = R.string.breeds_filter_error_retry_button_text),
            ) {
                breedsFilterViewModel.onRetryClicked()
            }
        is FilterScreenState.Ready ->
            ReadyScreen(
                state = state as FilterScreenState.Ready,
                modifier = modifier,
                onItemClicked = { breedsFilterViewModel.onBreedClicked(it) },
                onSelectedBreedsChanged = onSelectedBreedsChanged,
                onLoadMore = { breedsFilterViewModel.onLoadMoreBreeds() }
            )
    }
}

@Composable
internal fun ReadyScreen(
    modifier: Modifier = Modifier,
    state: FilterScreenState.Ready,
    onItemClicked: (BreedStateItem) -> Unit,
    onSelectedBreedsChanged: (List<BreedStateItem>) -> Unit,
    onLoadMore: () -> Unit
) {
    val checkedBreeds = state.breeds.filter { it.checked }
    LaunchedEffect(key1 = checkedBreeds.size) {
        onSelectedBreedsChanged(checkedBreeds)
    }
    Column(
        modifier = modifier.padding(PaddingMedium)
    ) {
        Text(
            text = stringResource(id = R.string.breeds_filter_title),
            style = MaterialTheme.typography.h6
        )
        val scrollState = rememberLazyListState()
        val shouldLoadMore = remember(key1 = scrollState, key2 = state.loadingMoreState) {
            derivedStateOf {
                (scrollState.firstVisibleItemIndex.toDouble() / state.breeds.size.toDouble()) >= 0.2 &&
                    (state.loadingMoreState as? LoadingMoreState.Idle)?.hasMore == true
            }
        }
        LaunchedEffect(key1 = shouldLoadMore.value) {
            if (shouldLoadMore.value) {
                onLoadMore()
            }
        }

        LazyColumn(
            modifier = modifier
                .weight(1f)
                .semantics { testTag = "breeds_list" },
            state = scrollState,
            contentPadding = PaddingValues(
                vertical = PaddingSmall
            )
        ) {
            items(state.breeds, key = { it.id.hashCode().plus(it.checked.hashCode()) }) { breed ->
                ListItem(
                    modifier = Modifier
                        .clickable { onItemClicked(breed) }
                        .semantics { testTag = "${breed.id}_holder" },
                    text = { Text(breed.name) },
                    icon = {
                        Checkbox(
                            checked = breed.checked,
                            onCheckedChange = { onItemClicked(breed) })
                    },
                    secondaryText = {
                        Column {
                            Text(
                                text = stringResource(
                                    id = R.string.breeds_filter_breed_origin_template,
                                    breed.origin
                                )
                            )
                            Text(
                                text = stringResource(
                                    id = R.string.breeds_filter_breed_lifespan_template,
                                    breed.lifeSpanState.lowerBound,
                                    breed.lifeSpanState.upperBound
                                )
                            )
                        }
                    },
                    singleLineSecondaryText = false
                )
            }
            if (state.loadingMoreState is LoadingMoreState.Loading) {
                item {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center
                    ) {
                        CircularProgressIndicator()
                    }
                }
            }

            if (state.loadingMoreState is LoadingMoreState.Error) {
                item {
                    ErrorScreen(
                        modifier = modifier.fillMaxWidth(),
                        errorMessage = stringResource(id = R.string.breeds_filter_error_message),
                        buttonText = stringResource(id = R.string.breeds_filter_error_retry_button_text),
                    ) {
                        onLoadMore()
                    }
                }
            }
        }
    }
}

