package me.amryousef.catsdogs.breed.filter

import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createComposeRule
import io.mockk.spyk
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import me.amryousef.breed.filter.presentation.BreedLifeSpanState
import me.amryousef.breed.filter.presentation.BreedStateItem
import me.amryousef.breed.filter.presentation.FilterScreenState
import me.amryousef.catsdogs.lib.presentation.LoadingMoreState
import org.junit.Rule
import org.junit.Test

class ReadyScreenTest {
    @get:Rule
    val composeRule = createComposeRule()

    @Test
    fun given_ready_state_with_breeds_when_ready_screen_rendered_then_list_items_are_visible() {
        val breed = BreedStateItem(
            checked = false,
            origin = "Origin",
            id = "id",
            name = "Name",
            lifeSpanState = BreedLifeSpanState(
                0,
                10
            )
        )
        val state = FilterScreenState.Ready(
            breeds = listOf(breed),
            currentPage = 1,
            loadingMoreState = LoadingMoreState.Idle(false)
        )
        composeRule.setContent {
            ReadyScreen(
                state = state,
                onItemClicked = {},
                onSelectedBreedsChanged = {},
                onLoadMore = {}
            )
        }
        composeRule.onNodeWithText(
            breed.name
        ).assertExists()
        composeRule.onNodeWithText(
            "Origin: ${breed.origin}"
        ).assertExists()
        composeRule.onNodeWithText(
            "Lifespan: ${breed.lifeSpanState.lowerBound} - ${breed.lifeSpanState.upperBound}"
        ).assertExists()
    }

    @ExperimentalTestApi
    @Test
    fun given_ready_state_with_loading_more_enable_when_ready_screen_rendered_and_scrolled_then_load_more_is_called() {
        val breeds = (0..29).map {
            BreedStateItem(
                checked = false,
                origin = "Origin-${it}",
                id = "id-${it}",
                name = "Name-${it}",
                lifeSpanState = BreedLifeSpanState(
                    0,
                    it
                )
            )
        }
        val state = FilterScreenState.Ready(
            breeds = breeds,
            currentPage = 0,
            loadingMoreState = LoadingMoreState.Idle(true)
        )
        val loadMoreLambda = spyk({})
        composeRule.setContent {
            ReadyScreen(
                state = state,
                onItemClicked = {},
                onSelectedBreedsChanged = {},
                onLoadMore = loadMoreLambda
            )
        }
        composeRule
            .onNodeWithTag("breeds_list")
            .performScrollToIndex(4)
        runBlocking { composeRule.awaitIdle() }
        verify(exactly = 0) { loadMoreLambda.invoke() }

        composeRule
            .onNodeWithTag("breeds_list")
            .performScrollToIndex(20)
        runBlocking { composeRule.awaitIdle() }
        verify(exactly = 1) { loadMoreLambda.invoke() }
    }

    @ExperimentalTestApi
    @Test
    fun given_ready_state_with_loading_more_disabled_when_ready_screen_rendered_and_scrolled_then_load_more_is_not_called() {
        val breeds = (0..29).map {
            BreedStateItem(
                checked = false,
                origin = "Origin-${it}",
                id = "id-${it}",
                name = "Name-${it}",
                lifeSpanState = BreedLifeSpanState(
                    0,
                    it
                )
            )
        }
        val state = FilterScreenState.Ready(
            breeds = breeds,
            currentPage = 0,
            loadingMoreState = LoadingMoreState.Idle(false)
        )
        val loadMoreLambda = spyk({})
        composeRule.setContent {
            ReadyScreen(
                state = state,
                onItemClicked = {},
                onSelectedBreedsChanged = {},
                onLoadMore = loadMoreLambda
            )
        }
        composeRule
            .onNodeWithTag("breeds_list")
            .performScrollToIndex(20)
        runBlocking { composeRule.awaitIdle() }
        verify(exactly = 0) { loadMoreLambda.invoke() }
    }

    @ExperimentalTestApi
    @Test
    fun given_ready_state_when_item_is_clicked_then_onItemClicked_is_called() {
        val breeds = listOf(
            BreedStateItem(
                checked = false,
                origin = "Origin",
                id = "id",
                name = "Name",
                lifeSpanState = BreedLifeSpanState(
                    0,
                    10
                )
            )
        )
        val state = FilterScreenState.Ready(
            breeds = breeds,
            currentPage = 0,
            loadingMoreState = LoadingMoreState.Idle(false)
        )
        val onItemClickedLambda = spyk<(BreedStateItem) -> Unit>({})
        composeRule.setContent {
            ReadyScreen(
                state = state,
                onItemClicked = onItemClickedLambda,
                onSelectedBreedsChanged = {},
                onLoadMore = {}
            )
        }
        composeRule
            .onNodeWithTag("id_holder")
            .performClick()
        runBlocking { composeRule.awaitIdle() }
        verify(exactly = 1) { onItemClickedLambda.invoke(breeds.first()) }
    }
}