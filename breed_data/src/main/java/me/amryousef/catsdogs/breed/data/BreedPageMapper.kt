package me.amryousef.catsdogs.breed.data

import me.amryousef.catsdogs.breed.domain.BreedPage
import javax.inject.Inject

class BreedPageMapper @Inject constructor(
    private val breedMapper: BreedMapper
) {
    fun map(response: List<BreedResponse>, totalCount: Int?): BreedPage {
        val mappedBreeds = breedMapper.map(response)
        return BreedPage(mappedBreeds, totalCount ?: Int.MAX_VALUE)
    }
}