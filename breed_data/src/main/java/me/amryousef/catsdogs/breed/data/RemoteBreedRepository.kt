package me.amryousef.catsdogs.breed.data

import kotlinx.coroutines.withContext
import me.amryousef.catsdogs.breed.domain.Breed
import me.amryousef.catsdogs.breed.domain.BreedPage
import me.amryousef.catsdogs.breed.domain.BreedRepository
import me.amryousef.catsdogs.lib.domain.DataFetchException
import me.amryousef.catsdogs.lib.domain.DispatchProvider
import javax.inject.Inject

class RemoteBreedRepository @Inject constructor(
    private val apiService: BreedApiService,
    private val breedMapper: BreedMapper,
    private val breedPageMapper: BreedPageMapper,
    private val dispatchProvider: DispatchProvider
) : BreedRepository {

    companion object {
        private const val PAGINATION_COUNT_HEADER_KEY = "Pagination-Count"
    }

    override suspend fun getBreeds(page: Int, limit: Int): BreedPage =
        withContext(dispatchProvider.io()) {
            val response = apiService.getBreeds(page, limit)
            if (response.isSuccessful) {
                breedPageMapper.map(
                    response.body() ?: throw DataFetchException(),
                    response.headers()[PAGINATION_COUNT_HEADER_KEY]?.toIntOrNull()
                )
            } else {
                throw DataFetchException()
            }
        }

    override suspend fun searchBreedsByName(query: String): List<Breed> =
        withContext(dispatchProvider.io()) {
            breedMapper.map(
                apiService.searchBreedsByName(query)
            )
        }
}