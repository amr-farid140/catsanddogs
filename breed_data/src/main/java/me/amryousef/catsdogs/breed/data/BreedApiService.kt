package me.amryousef.catsdogs.breed.data

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface BreedApiService {
    @GET("/v1/breeds")
    suspend fun getBreeds(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Response<List<BreedResponse>>

    @GET("/v1/breeds/search")
    suspend fun searchBreedsByName(
        @Query("q") query: String
    ): List<BreedResponse>
}