package me.amryousef.catsdogs.breed.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BreedResponse(
    @Json(name = "id")
    val id: String,
    @Json(name = "name")
    val name: String,
    @Json(name = "origin")
    val origin: String,
    @Json(name = "life_span")
    val lifeSpan: String
)