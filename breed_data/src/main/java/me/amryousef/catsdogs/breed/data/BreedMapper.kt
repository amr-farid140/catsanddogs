package me.amryousef.catsdogs.breed.data

import me.amryousef.catsdogs.breed.domain.Breed
import me.amryousef.catsdogs.breed.domain.BreedLifespan
import me.amryousef.catsdogs.lib.domain.DataProcessingException
import javax.inject.Inject

class BreedMapper @Inject constructor() {
    fun map(response: List<BreedResponse>): List<Breed> {
        return response.map { breedResponse ->
            Breed(
                id = breedResponse.id,
                name = breedResponse.name,
                origin = breedResponse.origin,
                lifespan = breedResponse.lifeSpan.toBreedLifespan()
            )
        }
    }

    private fun String.toBreedLifespan() =
        split("-")
            .mapNotNull {
                it.trim().toIntOrNull()
            }.run {
                if (size != 2) {
                    throw DataProcessingException()
                } else {
                    BreedLifespan(
                        lowerBound = get(0),
                        upperBound = get(1)
                    )
                }
            }
}