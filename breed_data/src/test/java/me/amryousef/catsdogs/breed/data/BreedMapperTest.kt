package me.amryousef.catsdogs.breed.data

import me.amryousef.catsdogs.breed.domain.Breed
import me.amryousef.catsdogs.breed.domain.BreedLifespan
import me.amryousef.catsdogs.lib.domain.DataProcessingException
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class BreedMapperTest {
    private lateinit var subject: BreedMapper

    @Before
    fun setup() {
        subject = BreedMapper()
    }

    @Test
    fun `given valid breed response when map then response is mapped`() {
        val input = listOf(
            BreedResponse(
                id = "1",
                name = "test",
                origin = "origin",
                lifeSpan = "1 - 2"
            ),
            BreedResponse(
                id = "1",
                name = "test",
                origin = "origin",
                lifeSpan = "1 - 2"
            )
        )
        val result = subject.map(input)

        assertEquals(
            expected = result.size,
            actual = 2
        )
        assertTrue {
            result.all {
                it == Breed(
                    id = "1",
                    name = "test",
                    origin = "origin",
                    lifespan = BreedLifespan(lowerBound = 1, upperBound = 2)
                )
            }
        }
    }

    @Test(expected = DataProcessingException::class)
    fun `given invalid breed lifespan when map then error is thrown`() {
        val input = listOf(
            BreedResponse(
                id = "1",
                name = "test",
                origin = "origin",
                lifeSpan = "1"
            )
        )
        subject.map(input)
    }
}