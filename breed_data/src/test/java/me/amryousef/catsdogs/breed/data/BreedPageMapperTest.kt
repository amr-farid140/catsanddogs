package me.amryousef.catsdogs.breed.data

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class BreedPageMapperTest {
    private lateinit var subject: BreedPageMapper
    private val mockBreedMapper = mockk<BreedMapper> {
        every { map(any()) } returns emptyList()
    }

    @Before
    fun setup() {
        subject = BreedPageMapper(mockBreedMapper)
    }

    @Test
    fun `given breeds response with total count when map then page is returned`() {
        val input = listOf(
            BreedResponse(
                id = "1",
                name = "test",
                origin = "origin",
                lifeSpan = "1"
            )
        )
        val result = subject.map(
            input,
            1
        )
        assertEquals(
            expected = 1,
            actual = result.totalCount
        )
        verify { mockBreedMapper.map(input) }
    }

    @Test
    fun `given breeds response without total count when map then page is returned`() {
        val input = listOf(
            BreedResponse(
                id = "1",
                name = "test",
                origin = "origin",
                lifeSpan = "1"
            )
        )
        val result = subject.map(
            input,
            null
        )
        assertEquals(
            expected = Int.MAX_VALUE,
            actual = result.totalCount
        )
        verify { mockBreedMapper.map(input) }
    }
}