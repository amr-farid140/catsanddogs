package me.amryousef.catsdogs.breed.data

import io.mockk.*
import kotlinx.coroutines.test.runBlockingTest
import me.amryousef.catsdogs.breed.domain.BreedPage
import me.amryousef.catsdogs.lib.domain.DataFetchException
import me.amryousef.catsdogs.lib.domain.DataProcessingException
import me.amryousef.catsdogs.lib.test.TestDispatcherProvider
import okhttp3.Headers
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import retrofit2.Response

class RemoteBreedRepositoryTest {
    private val mockApiService = mockk<BreedApiService>()
    private val mockBreedMapper = mockk<BreedMapper> {
        every { map(any()) } returns emptyList()
    }
    private val mockBreedPageMapper = mockk<BreedPageMapper>() {
        every { map(any(), any()) } returns BreedPage(emptyList(), 1)
    }
    private val testDispatchProvider = TestDispatcherProvider()
    private lateinit var subject: RemoteBreedRepository

    @Before
    fun setup() {
        subject = RemoteBreedRepository(
            apiService = mockApiService,
            breedMapper = mockBreedMapper,
            breedPageMapper = mockBreedPageMapper,
            dispatchProvider = testDispatchProvider
        )
    }

    @Test
    fun `given search query when searchBreedsByName then api service is used and response is mapped`() =
        runBlockingTest(testDispatchProvider.coroutineDispatcher) {
            coEvery {
                mockApiService.searchBreedsByName("test")
            }.returns(emptyList())
            subject.searchBreedsByName("test")
            coVerify { mockApiService.searchBreedsByName("test") }
            verify { mockBreedMapper.map(emptyList()) }
        }

    @Test(expected = DataProcessingException::class)
    fun `given breed mapper throws when searchBreedsByName then exception is propagated`() =
        runBlockingTest(testDispatchProvider.coroutineDispatcher) {
            coEvery {
                mockApiService.searchBreedsByName("test")
            }.returns(emptyList())
            every { mockBreedMapper.map(any()) }.throws(DataProcessingException())
            subject.searchBreedsByName("test")
        }

    @Test
    fun `given successful getBreeds response when getBreeds then api service is user and response is mapped`() {
        runBlockingTest(testDispatchProvider.coroutineDispatcher) {
            coEvery {
                mockApiService.getBreeds(any(), any())
            }.returns(
                Response.success(
                    emptyList(),
                    Headers.of(
                        mutableMapOf(
                            "Pagination-Count" to "1"
                        )
                    )
                )
            )
            subject.getBreeds(1, 10)
            verify { mockBreedPageMapper.map(any(), 1) }
            coVerify {
                mockApiService.getBreeds(1, 10)
            }
        }
    }

    @Test(expected = DataFetchException::class)
    fun `given unsuccessful getBreeds response when getBreeds then DataFetchException thrown`() {
        runBlockingTest(testDispatchProvider.coroutineDispatcher) {
            coEvery {
                mockApiService.getBreeds(any(), any())
            }.returns(
                Response.error(
                    400,
                    ResponseBody.create(MediaType.parse("text"), "test")
                )
            )
            subject.getBreeds(1, 10)
        }
    }

    @Test(expected = DataProcessingException::class)
    fun `given breed page mapper throws when getBreeds then exception is propagated`() {
        runBlockingTest(testDispatchProvider.coroutineDispatcher) {
            coEvery {
                mockApiService.getBreeds(any(), any())
            }.returns(
                Response.success(
                    emptyList(),
                    Headers.of(
                        mutableMapOf(
                            "Pagination-Count" to "1"
                        )
                    )
                )
            )
            every { mockBreedPageMapper.map(any(), any()) }
                .throws(DataProcessingException())
            subject.getBreeds(1, 10)
        }
    }
}